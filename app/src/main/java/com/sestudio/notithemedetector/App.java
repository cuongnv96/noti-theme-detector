package com.sestudio.notithemedetector;

import android.app.Application;
import android.content.Context;

import java.lang.ref.WeakReference;


// Created by cuongnv on 04/04/2019.

public class App extends Application {
    static WeakReference<Context> contextWeakReference;

    @Override
    public void onCreate() {
        super.onCreate();
        contextWeakReference = new WeakReference<>(getApplicationContext());
    }

    public static Context getAppContext() {
        return contextWeakReference != null ? contextWeakReference.get() : null;
    }
}
