package com.sestudio.notithemedetector.detector;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuongnv on 07/02/2018.
 */

public class NotiTheme implements INotiTheme {
    private static NotiTheme instance;

    public static NotiTheme getInstance() {
        if (instance == null) {
            instance = new NotiTheme();
        }
        return instance;
    }

    private INotiTheme currentTheme;

    private NotiTheme() {
        List<INotiTheme> notiThemes = new ArrayList<>();
        notiThemes.add(new NotiThemeCommon());
        notiThemes.add(new NotiThemeHuawei());
        for (INotiTheme notiTheme : notiThemes) {
            if (notiTheme.manufactor()) {
                currentTheme = notiTheme;
            }
        }
    }

    @Override
    public int getTheme(Context context) {
        if (currentTheme != null) {
            return this.currentTheme.getTheme(context);
        }
        return LIGHT;
    }

    @Override
    public boolean manufactor() {
        return this.currentTheme != null;
    }
}
