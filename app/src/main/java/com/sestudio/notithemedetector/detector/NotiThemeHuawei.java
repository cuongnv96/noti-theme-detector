package com.sestudio.notithemedetector.detector;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.widget.TextView;

/**
 * Created by cuongnv on 07/02/2018.
 */

public class NotiThemeHuawei extends NotiThemeCommon {
    public NotiThemeHuawei() {

    }

    public int getCurrentColor(Context context) {
        try {
            if (Build.VERSION.SDK_INT < 21) {
                return super.getCurrentColor(context);
            }
            int identifier = context.getResources().getIdentifier("android:layout/notification_template_material_base", null, null);
            if (identifier == 0) {
                return super.getCurrentColor(context);
            }
            @SuppressLint("ResourceType") TextView textView = (TextView) LayoutInflater.from(context).inflate(identifier, null).findViewById(android.R.id.title);
            if (textView == null) {
                return super.getCurrentColor(context);
            }
            return textView.getTextColors().getDefaultColor();
        }
        catch (Exception e) {
            return super.getCurrentColor(context);
        }
    }

    @Override
    public boolean manufactor() {
        return Build.MANUFACTURER.toLowerCase().contains("huawei");
    }
}
