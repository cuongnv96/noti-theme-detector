package com.sestudio.notithemedetector.detector;

import android.graphics.Color;

/**
 * Created by cuongnv on 07/02/2018.
 */

public class NotiThemeUtils {
    public static final int DEFAULT_NOTIF_BACKGROUND_COLOR = 0xffffffff; // light

    public static boolean isLightTheme(int color) {
        return 1.0d - ((((0.299d * ((double) Color.red(color))) + (0.587d * ((double) Color.green(color)))) + (0.114d * ((double) Color.blue(color)))) / 255.0d) > 0.5d;
    }
}
