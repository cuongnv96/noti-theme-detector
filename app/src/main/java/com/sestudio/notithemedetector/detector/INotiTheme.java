package com.sestudio.notithemedetector.detector;

import android.content.Context;

/**
 * Created by cuongnv on 07/02/2018.
 */

public interface INotiTheme {
    int LIGHT = 0;
    int DARK = 1;

    int getTheme(Context context);

    boolean manufactor();
}
