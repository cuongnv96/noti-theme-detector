package com.sestudio.notithemedetector.detector;

// Created by cuongnv on 04/04/2019.

import android.app.Notification;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Detector {
    Context context;

    public Detector(Context context) {
        this.context = context;
    }

    private Integer notification_text_color = null;
    private float notification_text_size = 11;
    private final String COLOR_SEARCH_RECURSE_TIP = "SOME_SAMPLE_TEXT";

    private boolean recurseGroup(ViewGroup gp) {
        final int count = gp.getChildCount();
        for (int i = 0; i < count; ++i) {
            if (gp.getChildAt(i) instanceof TextView) {
                final TextView text = (TextView) gp.getChildAt(i);
                final String szText = text.getText().toString();
                if (COLOR_SEARCH_RECURSE_TIP.equals(szText)) {
                    notification_text_color = text.getTextColors().getDefaultColor();
                    notification_text_size = text.getTextSize();
                    DisplayMetrics metrics = new DisplayMetrics();
                    WindowManager systemWM = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                    systemWM.getDefaultDisplay().getMetrics(metrics);
                    notification_text_size /= metrics.scaledDensity;
                    return true;
                }
            } else if (gp.getChildAt(i) instanceof ViewGroup)
                return recurseGroup((ViewGroup) gp.getChildAt(i));
        }
        return false;
    }

    public void extractColors() {
        if (notification_text_color != null)
            return;

        try {

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "");
            builder.setContentTitle("tite");
            builder.setContentText("text");

            Notification ntf = builder.build();
//            ntf.setLatestEventInfo(this, COLOR_SEARCH_RECURSE_TIP, "Utest", null);
            LinearLayout group = new LinearLayout(context);
            ViewGroup event = (ViewGroup) ntf.contentView.apply(context, group);
            recurseGroup(event);
            group.removeAllViews();
        } catch (Exception e) {
            notification_text_color = android.R.color.black;
        }
    }

    public int getColor() {
        return notification_text_color;
    }
}
