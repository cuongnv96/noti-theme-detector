package com.sestudio.notithemedetector.detector;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;

/**
 * Created by cuongnv on 07/02/2018.
 */

public class NotiThemeCommon implements INotiTheme {
    @Override
    public int getTheme(Context context) {
        if (NotiThemeUtils.isLightTheme(getCurrentColor(context))) {
            return LIGHT;
        }
        return DARK;
    }

    public int getCurrentColor(Context context) {
        int id;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            id = android.R.style.TextAppearance_Material_Notification_Title;
        } else {
            id = android.R.style.TextAppearance_StatusBar_EventContent_Title;
        }
        return getColorWithId(context, id, NotiThemeUtils.DEFAULT_NOTIF_BACKGROUND_COLOR);
    }

    private int getColorWithId(Context context, int id, int fallback) {
        try {
            TypedArray ta = context.obtainStyledAttributes(id, new int[]{
                    android.R.attr.textColor
            });
            int color = ta.getColor(0, fallback);
            ta.recycle();
            return color;
        } catch (Exception e) {
            return fallback;
        }
    }

    @Override
    public boolean manufactor() {
        return true;
    }
}
