package com.sestudio.notithemedetector;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sestudio.notithemedetector.detector.Detector;
import com.sestudio.notithemedetector.detector.NotiTheme;
import com.sestudio.notithemedetector.detector.NotiThemeUtils;

public class MainActivity extends AppCompatActivity {
    View container;
    TextView content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        container = findViewById(R.id.container);
        content = findViewById(R.id.content);

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detect();
            }
        });
    }

    private void detect() {
        Context context = getApplicationContext();
        Resources resources = context.getResources();
        LinearLayout parent = findViewById(R.id.parentview);


//        int rootId = resources.getIdentifier("com.bkav.bphone.bms:id/texttitnotify", null, null);
//        if (rootId != 0) {
//            View view = LayoutInflater.from(context).inflate(rootId, parent, false);
//            parent.addView(view);
//        }
//
//        Detector detector = new Detector(getApplicationContext());
//        detector.extractColors();
//
//        boolean isLight = NotiThemeUtils.isLightTheme(detector.getColor());

        boolean isLight = NotiTheme.getInstance().getTheme(getApplicationContext()) == NotiTheme.LIGHT;

        if (!isLight) {
            container.setBackgroundColor(0xff000000);
            content.setTextColor(0xffffffff);
        } else {
            container.setBackgroundColor(0xffffffff);
            content.setTextColor(0xff000000);
        }
        int layoutid = resources.getIdentifier("android:layout/notification_template_material_base", null, null);
        if (layoutid != 0) {
            View view = LayoutInflater.from(context).inflate(layoutid, parent, false);
            parent.addView(view);

            TextView title = view.findViewById(android.R.id.title);
            title.setText("Use USB for hello title");

            TextView textLine1 = view.findViewById(findSystemId("text_line_1"));
            if (textLine1 != null) textLine1.setText("Use USB for hello text_line_1");

            TextView text = view.findViewById(findSystemId("text"));
            if (text != null) text.setText("Charging hello text");
        }


        int layoutbig = resources.getIdentifier("android:layout/notification_template_material_big_base", null, null);
        if (layoutid != 0) {
            View view = LayoutInflater.from(context).inflate(layoutbig, parent, false);
            parent.addView(view);
        }

        int bgId = resources.getIdentifier("com.android.systemui:id/backgroundNormal", null, null);
        if (bgId != 0) {
            View background = LayoutInflater.from(context).inflate(bgId, parent, false);
            background.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200));
            parent.addView(background);
        }
    }

    public int findSystemId(String id) {
        Resources resources = getResources();
        return resources.getIdentifier("android:id/" + id, null, null);
    }
}
