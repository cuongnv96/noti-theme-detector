# Notification Theme Detector:

Detect trên các phone chung:
<br/>
<img src="./pics/common.png" width="500" height="auto">

Detect trên phone huawei:
<br/>
<img src="./pics/huawei.png" width="900" height="auto">

## Issues:

Trên BPhone 2 (2017) BOS 2.4 (android 7.1.2) các values trong notification system resources luôn báo device đang sử dụng light theme, nhưng BPhone đang sử dụng dark theme